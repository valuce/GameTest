﻿using UnityEngine;
using GameTest.Data;

namespace GameTest
{
	[ExecuteInEditMode]
	public class LevelNexusEdit : MonoBehaviour
	{
		public ShipsData shipsData;		
		public LevelNexus levelNexus;

		public void Save()
		{
			var ships = FindObjectsOfType<ShipComponent>();

			levelNexus.enemies.Clear();
			foreach( var ship in ships )
			{
				levelNexus.enemies.Add( new EnemyLevelPositionData()
				{
					enemyName = ship.shipType,
					position = ship.transform.position,
				} );
			}

#if UNITY_EDITOR
			UnityEditor.EditorUtility.SetDirty( levelNexus );
#endif
		}

		public void Load()
		{
			Clear();

			ShipsManager shipsManager = new ShipsManager();
			shipsManager.Init( shipsData );
			foreach( var data in levelNexus.enemies )
			{
				Instantiate( shipsManager.GetShipData(data.enemyName).Prefab, data.position, Quaternion.Euler( Vector3.forward * 180f ) );
			}
		}

		public void Clear()
		{
			var ships = FindObjectsOfType<ShipComponent>();
			foreach( var ship in ships )
				DestroyImmediate( ship.gameObject );
		}
	}
}