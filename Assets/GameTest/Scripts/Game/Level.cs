﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameTest
{

	public class Level : MonoBehaviour
	{
		[SerializeField]
		private Transform _playerStartPoint;
		
		private EnemiesGroup _enemiesGroup;
		private GameObject _player;

		private void Awake()
		{
			_enemiesGroup = GetComponentInChildren<EnemiesGroup>();
			_enemiesGroup.onAllPlayerDead.AddListener( LevelComplete );
		}

		private void Start()
		{
			LoadLevel();

			CreatePlayer();
		}

		private void LevelComplete()
		{
			GameData.Instance.currentLevel++;
			GameController.Instance.GotoState( GameState.LevelWIn );			
		}

		private void LevelLose()
		{
			GameController.Instance.GotoState( GameState.GameOver );			
		}

		private void CreatePlayer()
		{
			var shipData = GameData.Instance.ships.GetShipData( GameData.Instance.selectedPlayer );

			_player = GameObjectsPool.Instantiate( shipData.Prefab, transform, false );
			_player.transform.position = _playerStartPoint.position;
			GameEvents.PlayerCreated( _player );
			
			var hp= _player.GetComponent<HealthComponent>();
			if( hp )
				hp.onDie.AddListener( LevelLose );
		}

		private void LoadLevel()
		{
			var ships = GameData.Instance.ships;
			var level = GameData.Instance.levels.GetLevel( GameData.Instance.currentLevel );

			Instantiate( level.background, transform, false );

			foreach( var enemyLevelData in level.nexus.enemies )
			{
				var shipData = ships.GetShipData( enemyLevelData.enemyName );
				if( shipData != null )
					_enemiesGroup.CreateEnemy( shipData.Prefab, enemyLevelData.position );
			}
		}
	}
}