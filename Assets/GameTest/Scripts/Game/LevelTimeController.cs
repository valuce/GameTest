﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameTest
{
	public class LevelTimeController : MonoBehaviour
	{
		const float FX_TIME = 2f;

		private float _timer = FX_TIME;
		private bool isActive { get { return _timer < FX_TIME; } }

		private float _scaleMin = 0f;
		private float _scaleMax = 1f;

		private void Start()
		{
			ResetTime();

			GameController.Instance.onChangeGameState += OnChangeGameState;
		}

		private void OnChangeGameState( GameState prevState, GameState newState )
		{
			switch( newState )
			{
				case GameState.GameOver:
				case GameState.LevelWIn:
					SlowTime();
					break;
			}

		}

		private void OnDestroy()
		{
			ResetTime();
			GameController.Instance.onChangeGameState -= OnChangeGameState;
		}

		private void ResetTime()
		{
			Time.timeScale = 1;
		}

		private void SlowTime()
		{
			_scaleMin = 0f;
			_scaleMax = 1f;
			_timer = 0;
		}

		private void Update()
		{
			if( isActive )
			{
				_timer += Time.unscaledDeltaTime;

				Time.timeScale = Mathf.Lerp( _scaleMin, _scaleMax, 1f - _timer / FX_TIME );
			}
		}
	}
}