﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameTest
{
	[RequireComponent(typeof(EnemiesGroup))]
	public class EnemiesGroupMovement : MonoBehaviour
	{
		[SerializeField]
		private float _speed = 1f;
		private Vector3 _direction = Vector3.right;
		private EnemiesGroup _group;
		private Transform _groupTransform;

		[SerializeField]
		private Vector2 _limit;

		private void Awake()
		{
			_group = GetComponent<EnemiesGroup>();
			_groupTransform = _group.transform;
		}

		private void Update()
		{
			_groupTransform.localPosition += _direction * Time.deltaTime * _speed;

			Rect groupBounds = _group.GetBounds();

			float defMin = -_limit.x - groupBounds.xMin;
			float defMax = _limit.x - groupBounds.xMax;

			if( defMax <= 0f )
			{
				_direction.x = -1f;
				//_groupTransform.localPosition -= _direction * defMax;
			}
			else if( defMin >= 0f )
			{
				_direction.x = 1f;
				//_groupTransform.localPosition -= _direction * defMin;
			}
		}
	}
}