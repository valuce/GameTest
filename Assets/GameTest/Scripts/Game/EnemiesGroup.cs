﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace GameTest
{

	public class EnemiesGroup : MonoBehaviour
	{
		[SerializeField]
		private UnityEvent _onAllPlayerDead;
		public UnityEvent onAllPlayerDead { get { return _onAllPlayerDead; } }

		private int _enemiesCount = 0;

		public Rect GetBounds()
		{
			Rect bounds = new Rect();
			if( transform.childCount > 0 )
			{
				Transform child = transform.GetChild( 0 );
				bounds.Set( child.position.x, child.position.y, 1, 1 );
			}

			foreach( Transform child in transform )
			{
				bounds.xMin = Mathf.Min( bounds.xMin, child.position.x );
				bounds.xMax = Mathf.Max( bounds.xMax, child.position.x );
				bounds.yMin = Mathf.Max( bounds.yMin, child.position.y );
				bounds.yMax = Mathf.Min( bounds.yMax, child.position.y );
			}

			return bounds;
		}

		public void CreateEnemy( GameObject Prefab, Vector3 position )
		{
			_enemiesCount++;

			GameObject enemy = Instantiate( Prefab, transform, false );
			enemy.transform.position = position;
			enemy.transform.eulerAngles = Vector3.forward * 180f;

			var hp = enemy.GetComponent<HealthComponent>();
			if( hp )
				hp.onDie.AddListener( OnEnemyDie );
		}

		private void OnEnemyDie()
		{
			if( --_enemiesCount <= 0 )
			{
				onAllPlayerDead.Invoke();
			}
		}
	}
}