﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace GameTest
{

	public class UIBonusUseItem : MonoBehaviour
	{

		[SerializeField]
		private Slider _slider;

		private BonusUseComponent _useComponent;

		public void Init( BonusUseComponent useComponent )
		{
			_useComponent = useComponent;
			Refresh();

			if( _useComponent )
			{
				gameObject.SetActive( true );
				_useComponent.onStopUse += Disable;
			}
		}

		private void OnDisable()
		{
			if( _useComponent )
			{
				_useComponent.onStopUse -= Disable;
				_useComponent = null;
			}
		}

		private void LateUpdate()
		{
			Refresh();
		}

		public void Refresh()
		{
			if( _slider && _useComponent )
				_slider.value = _useComponent.useRatio;
		}

		public void Disable()
		{
			gameObject.SetActive( false );
		}
	}
}