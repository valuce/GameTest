﻿using UnityEngine;
using System.Collections.Generic;

namespace GameTest
{

	public class UIPlayerHUD : MonoBehaviour
	{
		[SerializeField]
		private UIHealth _health;
		private GameObject _player;
		private List<UIBonusUseItem> _bonuses = new List<UIBonusUseItem>();

		private void Awake()
		{
			GetComponentsInChildren( true, _bonuses );
		}

		private void Start()
		{
			foreach( var item in _bonuses )
			{
				item.gameObject.SetActive( false );
			}
		}

		private void OnEnable()
		{
			GameEvents.onPlayerCreated += RegisterPlayer;
		}

		private void OnDisable()
		{
			GameEvents.onPlayerCreated -= RegisterPlayer;
			UnregisterPlayer();
		}

		private void RegisterPlayer( GameObject player )
		{
			if( player == null )
				return;

			_player = player;

			if( _health )
				_health.Init( _player.GetComponent<HealthComponent>() );

			var bonusReceivier = _player.GetComponent<BonusReceiverComponent>();
			if( bonusReceivier )
				bonusReceivier.onUseBonus += OnUseBonus;
		}

		private void UnregisterPlayer()
		{
			if( _player == null )
				return;

			var bonusReceivier = _player.GetComponent<BonusReceiverComponent>();
			if( bonusReceivier )
				bonusReceivier.onUseBonus -= OnUseBonus;
		}



		private void OnUseBonus( BonusUseComponent bonusUse )
		{
			if( bonusUse is BonusUseMovingSpeed )
			{
				ShowBonus( "MovingSpeed", bonusUse );
			}
			else if( bonusUse is BonusUseShootingSpeed )
			{
				ShowBonus( "ShootingSpeed", bonusUse );
			}
			else if( bonusUse is BonusUseShield )
			{
				ShowBonus( "Shield", bonusUse );
			}
		}

		private void ShowBonus( string name, BonusUseComponent bonusUse )
		{
			foreach( var item in _bonuses )
			{
				if( item.name == name )
				{
					item.Init( bonusUse );										
					break;
				}
			}
		}
	}
}