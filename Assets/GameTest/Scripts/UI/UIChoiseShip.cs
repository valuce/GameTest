﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GameTest
{

	public class UIChoiseShip : MonoBehaviour
	{
		[SerializeField]
		private Transform _ships;
		[SerializeField]
		private Button _prevBtn;
		[SerializeField]
		private Button _nextBtn;
		[SerializeField]
		private Text _detailsText;

		private int _currentShipIndex = -1;
		private List<Data.ShipData> _shipDatas;

		

		private int shipsCount
		{
			get
			{
				return _ships ? _ships.childCount : 0;
			}
		}

		private void Awake()
		{
			if( _nextBtn )
				_nextBtn.onClick.AddListener( OnClick_NextBtn );
			if( _prevBtn )
				_prevBtn.onClick.AddListener( OnClick_PrevBtn );
		}

		private void Start()
		{
			_shipDatas = GameData.Instance.ships.GetShipsWithType( "Player" );

			foreach( var data in _shipDatas )
			{
				var Prefab = Assets.GetAsset<GameObject>( data.name, "Lobby" );
				var go = Instantiate( Prefab, _ships, false );
				go.transform.localPosition = Vector3.forward * -100f;
			}

			if( _shipDatas.Count > 0 )
			{
				_currentShipIndex = 0;
				for( int i = 0; i < _shipDatas.Count; ++i )
				{
					if( _shipDatas[i].name == GameData.Instance.selectedPlayer )
					{
						_currentShipIndex = i;
						break;
					}
				}
			}

			Refresh();
		}

		private void OnClick_NextBtn()
		{
			_currentShipIndex = Mathf.Clamp( _currentShipIndex + 1, 0, shipsCount - 1 );
			Refresh();
		}

		private void OnClick_PrevBtn()
		{
			_currentShipIndex = Mathf.Clamp( _currentShipIndex - 1, 0, shipsCount - 1 );
			Refresh();
		}

		private void Refresh()
		{
			if( _nextBtn )
				_nextBtn.interactable = _currentShipIndex < shipsCount - 1;
			if( _prevBtn )
				_prevBtn.interactable = _currentShipIndex > 0;

			if( _ships )
			{
				for( int i = 0, i_end = shipsCount; i < i_end; ++i )
				{
					var child = _ships.GetChild( i );
					child.gameObject.SetActive( i == _currentShipIndex );
				}
			}

			if( _currentShipIndex >= 0 && _currentShipIndex < _shipDatas.Count )
			{
				var ship = _shipDatas[_currentShipIndex];

				GameData.Instance.selectedPlayer = ship.name;

				if( _detailsText )
				{
					ShipStats stats = GameData.Instance.stats.GetShipStats( ship.type, ship.multipliers );
					_detailsText.text = string.Format(
						"Health: {0}\n" +
						"Moving Speed: {1}\n" +
						"Shooting Speed: {2}\n"+
						"Mobility: {3}\n"+
						"Weapon Damage: {4}\n", stats.health, stats.moveSpeed, (int)(stats.shootSpeed * 1000f), stats.mobility, stats.weaponDamage );
				}
			}
		}
	}
}