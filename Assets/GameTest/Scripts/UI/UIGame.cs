﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GameTest
{

	public class UIGame : MonoBehaviour
	{
		[SerializeField]
		private GameObject _pausePanel;
		[SerializeField]
		private GameObject _settingsPanel;
		[SerializeField]
		private GameObject _gameOverPanel;
		[SerializeField]
		private GameObject _levelWinPanel;
		[SerializeField]
		private Text _levelNumText;
		[SerializeField]
		private GameObject _nextLevelBtn;


		private void Awake()
		{
			SafeActive( _pausePanel, false );
			SafeActive( _settingsPanel, false );
			SafeActive( _gameOverPanel, false );
			SafeActive( _levelWinPanel, false );			
		}

		private void Start()
		{
			if( _levelNumText )
				_levelNumText.text = string.Format( "Level {0}", GameData.Instance.currentLevel + 1 );
		}

		private void OnEnable()
		{
			GameController.Instance.onChangeGameState += OnChangeGameState;
		}

		private void OnChangeGameState( GameState prevState, GameState newState )
		{
			if( _nextLevelBtn )
				_nextLevelBtn.SetActive( GameData.Instance.currentLevel < GameData.Instance.levels.levelsCount );

			switch( newState )
			{
				case GameState.GameOver:
					StartCoroutine( _CallDelay( OnGameOver, 2f ) );					
					break;
				case GameState.LevelWIn:
					StartCoroutine( _CallDelay( OnLevelComplete, 2f ) );
					break;
			}
		}

		private IEnumerator _CallDelay( System.Action action, float delay )
		{
			yield return new WaitForSecondsRealtime(delay);
			if( action != null )
				action.Invoke();
			yield break;
		}
	

		private void OnDisable()
		{
			GameController.Instance.onChangeGameState -= OnChangeGameState;
		}

		private void OnGameOver()
		{
			SafeActive( _gameOverPanel, true );			
		}

		private void OnLevelComplete()
		{
			SafeActive( _levelWinPanel, true );			
		}

		private void SafeActive( GameObject gameObject, bool active )
		{
			if( gameObject )
				gameObject.SetActive( active );
		}

		public void OnClick_SettingsBtn()
		{
			SafeActive( _settingsPanel, true );

			SafeActive( _pausePanel, false );
		}

		public void OnClick_SettingsCloseBtn()
		{
			SafeActive( _settingsPanel, false );

			SafeActive( _pausePanel, true );
		}

		public void OnClick_ResumeBtn()
		{
			GamePause( false );
			SafeActive( _pausePanel, false );
		}

		public void OnClick_PauseBtn()
		{
			if( GameController.Instance.currentGameState == GameState.Game )
			{
				GamePause( true );
				SafeActive( _pausePanel, true );
			}
		}

		public void OnClick_RestartBtn()
		{
			GameController.Instance.GotoState( GameState.Game );
		}

		public void OnClick_BackToLobbyBtn()
		{
			GameController.Instance.GotoState( GameState.Lobby );
		}

		public void OnClick_BackBtn()
		{
			GameController.Instance.GotoState( GameState.MainMenu );
		}

		public void OnClick_QuitBtn()
		{
			GameController.Instance.Quit();
		}

		private void GamePause( bool pause )
		{
			Time.timeScale = pause ? 0f : 1f;
		}
	}
}