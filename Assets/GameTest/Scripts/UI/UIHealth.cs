﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace GameTest
{
	public class UIHealth : MonoBehaviour
	{
		[SerializeField]
		private Slider _slider;

		private HealthComponent _health;

		public void Init( HealthComponent health )
		{
			_health = health;
			Refresh();
		}

		private void LateUpdate()
		{
			Refresh();
		}

		public void Refresh()
		{
			if( _slider && _health )
				_slider.value = _health.healthRatio;
		}
	}
}