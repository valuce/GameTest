﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GameTest
{

	public class UISettings : MonoBehaviour
	{
		[SerializeField]
		private Slider _musicSlider;
		[SerializeField]
		private Slider _sfxSlider;

		private void Start()
		{
			if( _musicSlider )
			{
				_musicSlider.value = SoundsManager.Instance.musicVolume;
				_musicSlider.onValueChanged.AddListener( OnValueChanged_MusicSlider );
			}

			if( _sfxSlider )
			{
				_sfxSlider.value = SoundsManager.Instance.sfxVolume;
				_sfxSlider.onValueChanged.AddListener( OnValueChanged_SfxSlider );
			}
		}

		
		private void OnValueChanged_MusicSlider( float value )
		{
			if( _musicSlider )
				_musicSlider.value = value;

			SoundsManager.Instance.musicVolume = value;
		}

		private void OnValueChanged_SfxSlider( float value )
		{
			if( _sfxSlider )
				_sfxSlider.value = value;

			SoundsManager.Instance.sfxVolume = value;
		}
	}
}