﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameTest
{

	public class UIMainMenu : MonoBehaviour
	{
		[SerializeField]
		private GameObject _settingsPanel;

		private void Awake()
		{
			if( _settingsPanel )
				_settingsPanel.SetActive( false );
		}

		public void OnClick_PlayBtn()
		{
			GameController.Instance.GotoState( GameState.Lobby );
		}

		public void OnClick_SettingsBtn()
		{
			if( _settingsPanel )
				_settingsPanel.SetActive( true );
		}

		public void OnClick_QuitBtn()
		{
			GameController.Instance.Quit();
		}
	}
}