﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameTest
{

	public class UILobby : MonoBehaviour
	{
		public void OnClick_BackBtn()
		{
			GameController.Instance.GotoState( GameState.MainMenu );
		}

		public void OnClick_StartBtn()
		{
			GameController.Instance.GotoState( GameState.Game );
		}
	}
}