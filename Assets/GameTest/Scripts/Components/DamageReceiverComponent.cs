﻿using UnityEngine;
using System.Collections;

namespace GameTest
{

	public class DamageReceiverComponent : MonoBehaviour
	{

		virtual public bool TakeDamage( float amount )
		{
			HealthComponent hp = GetComponent<HealthComponent>();
			if( hp && !hp.isDead )
			{
				hp.TakeDamage( amount );
				return true;
			}

			return false;
		}
	}
}