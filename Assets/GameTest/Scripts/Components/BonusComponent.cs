﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace GameTest
{
	public enum BonusType
	{
		Health, MovingSpeed, ShootingSpeed, Shield,
	}

	public class BonusComponent : MonoBehaviour
	{
		[SerializeField]
		private BonusType _bonusType;
		[SerializeField]
		private float _useTime;
		[SerializeField]
		private float _value;

		[SerializeField]
		private UnityEvent _onPickUp;
		public UnityEvent onPickUp { get { return _onPickUp; } }

		private void OnTriggerEnter2D( Collider2D collision )
		{
			BonusReceiverComponent receiver = collision.GetComponent<BonusReceiverComponent>();
			if( receiver != null )
			{
				receiver.ApplyBonus( _bonusType, _useTime, _value );
				onPickUp.Invoke();
				GameObjectsPool.Destroy( gameObject );
			}
		}
	}
}