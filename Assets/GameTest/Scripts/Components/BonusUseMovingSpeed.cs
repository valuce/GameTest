﻿using UnityEngine;
using System.Collections;

namespace GameTest
{

	public class BonusUseMovingSpeed : BonusUseComponent
	{
		private MovementComponent _movement;
		private float _prevValue;

		private void Awake()
		{
			_movement = GetComponent<MovementComponent>();
		}

		public override void Use( float useTime, float value )
		{
			base.Use( useTime, value );

			if( _movement )
			{
				_prevValue = _movement.speed;
				_movement.speed *= value;
			}
		}

		protected override void OnStopUse()
		{
			base.OnStopUse();

			if( _movement )
				_movement.speed = _prevValue;
		}
	}
}