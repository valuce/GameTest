﻿using UnityEngine;
using System.Collections;

namespace GameTest
{

	public class MovementComponent : MonoBehaviour
	{
		public float speed = 1f;
		
		virtual public void Move( Vector3 direction )
		{
			transform.Translate( direction * speed * Time.deltaTime );
		}
	}
}