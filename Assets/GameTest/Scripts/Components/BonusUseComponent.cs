﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

namespace GameTest
{

	public class BonusUseComponent : MonoBehaviour
	{
		public event UnityAction onStopUse;

		private float _timer = 0f;		
		private float _useTime;

		public float useRatio
		{
			get { return 1f - Mathf.Clamp01(_timer / _useTime); }
		}

		private void Start()
		{
			Reset();
		}


		public void Reset()
		{
			_timer = 0;
		}

		virtual public void Use( float useTime, float value )
		{
			_useTime = useTime;
		}

		virtual protected void OnStopUse()
		{
			if( onStopUse != null )
				onStopUse.Invoke();

			Destroy( this );
		}

		private void Update()
		{
			_timer += Time.deltaTime;
			if( _timer >= _useTime )
			{
				OnStopUse();
			}
		}
	}
}