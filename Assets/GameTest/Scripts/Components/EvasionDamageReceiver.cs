﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

namespace GameTest
{

	public class EvasionDamageReceiver : DamageReceiverComponent
	{
		[SerializeField]
		[Range(0f, 1f)]
		private float _chance = 0f;

		[SerializeField]
		private UnityEvent _onDodged;
		public UnityEvent onDodged { get { return _onDodged; } }

		public void Init( float chance )
		{
			_chance = Mathf.Clamp01( chance );
		}

		public override bool TakeDamage( float amount )
		{
			if( Random.value > _chance )
				return base.TakeDamage( amount );

			onDodged.Invoke();
			return false;
		}
	}
}