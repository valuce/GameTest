﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

namespace GameTest
{

	public class BonusReceiverComponent : MonoBehaviour
	{
		public event UnityAction<BonusUseComponent> onUseBonus;

		public void ApplyBonus( BonusType bonusType, float useTime, float value )
		{
			switch( bonusType )
			{
				case BonusType.Health:
					var hp = GetComponent<HealthComponent>();
					if( hp )
						hp.AddHealth( value );
					break;

				case BonusType.MovingSpeed:
					Use<BonusUseMovingSpeed>( useTime, value );
					break;

				case BonusType.ShootingSpeed:
					Use<BonusUseShootingSpeed>( useTime, value );
					break;

				case BonusType.Shield:
					Use<BonusUseShield>( useTime, value );
					break;

				default:
					break;
			}
		}

		private void Use<T>( float useTime, float value ) where T : BonusUseComponent
		{
			T component = gameObject.GetComponent<T>();
			if( component )
			{
				component.Reset();
			}
			else
			{
				component = gameObject.AddComponent<T>();
				component.Use( useTime, value );

				if( onUseBonus != null )
					onUseBonus.Invoke( component );
			}
		}
	}
}