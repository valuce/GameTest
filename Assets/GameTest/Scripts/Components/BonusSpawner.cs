﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameTest
{

	public class BonusSpawner : MonoBehaviour
	{
		[Range( 0, 100)]
		[SerializeField]
		private int _chance = 0;

		public void TrySpawn()
		{
			if( _chance > Random.Range( 0, 100 ) )
			{
				GameObject Prefab = GameData.Instance.bonuses.GetRandomBonus();
				if( Prefab )
					GameObjectsPool.Instantiate( Prefab, transform.position, Quaternion.identity );
			}
		}
	}
}