﻿using UnityEngine;
using System.Collections;

namespace GameTest
{

	public class EnemyAttackComponent : MonoBehaviour
	{
		[SerializeField]
		private LayerMask _layer;
		private Collider2D _collider;
		private WeaponComponent _weapon;
		private RaycastHit2D[] _raycastHits = new RaycastHit2D[1];

		private void Awake()
		{
			_collider = GetComponent<Collider2D>();
			_weapon = GetComponent<WeaponComponent>();
		}

		private void Start()
		{
			Invoke( "TryShoot", 3f + Random.value * _weapon.shootDelay );
		}

		private void TryShoot()
		{
			Vector2 direction = transform.rotation * Vector3.up;
			int count = _collider.Raycast( direction, _raycastHits, 10f, _layer.value);
			if( count == 0 )
			{
				_weapon.Shoot();
			}

			Invoke( "TryShoot", Random.Range(_weapon.shootDelay, _weapon.shootDelay + 1f ) );
		}
	}
}