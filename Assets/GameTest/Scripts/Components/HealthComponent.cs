﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace GameTest
{
	public class HealthComponent : MonoBehaviour
	{
		[SerializeField]
		private float _healthMax = 100f;
		private float _health = 0;


		[Header( "Events" )]
		[SerializeField]
		private UnityEvent _onDamage;
		public UnityEvent onDamage { get { return _onDamage; } }

		[SerializeField]
		private UnityEvent _onDie;
		public UnityEvent onDie { get { return _onDie; } }

		public float healthRatio
		{
			get { return _health / _healthMax; }
		}


		public bool isDead
		{
			get { return _health <= 0; }
		}

		private void Awake()
		{
			_health = _healthMax;
		}

		public void Init( float health )
		{
			if( health > 0f )
				_health = _healthMax = health;
		}

		public void AddHealth( float amount )
		{
			if( amount > 0f )
				_health = Mathf.Max( _health + amount, _healthMax );
		}

		public void TakeDamage( float damage )
		{
			if( isDead )
				return;

			if( damage <= 0f )
				return;

			_health = Mathf.Max( 0f, _health - damage );

			OnDamage( damage );

			if( isDead )
			{
				OnDie();
			}

			return;
		}

		virtual protected void OnDamage( float damage )
		{
			onDamage.Invoke();
		}

		virtual protected void OnDie()
		{
			onDie.Invoke();
		}
	}
}