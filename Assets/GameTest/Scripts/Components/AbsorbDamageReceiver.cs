﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameTest
{

	public class AbsorbDamageReceiver : DamageReceiverComponent
	{
		public override bool TakeDamage( float amount )
		{
			return true;
		}

	}
}