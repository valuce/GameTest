﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameTest
{
	[RequireComponent( typeof( HealthComponent ))]
	[RequireComponent( typeof( EvasionDamageReceiver ) )]
	[RequireComponent( typeof( MovementComponent ) )]
	[RequireComponent( typeof( WeaponComponent ) )]
	public class ShipComponent : MonoBehaviour
	{
		[SerializeField]
		private string _shipType;
		public string shipType { get { return _shipType; } }

		private HealthComponent _health;
		private EvasionDamageReceiver _evasionDamageReceiver;
		private MovementComponent _movement;
		private WeaponComponent _weapon;

		private void Awake()
		{
			_health = GetComponent<HealthComponent>();
			_evasionDamageReceiver = GetComponent<EvasionDamageReceiver>();
			_movement = GetComponent<MovementComponent>();
			_weapon = GetComponent<WeaponComponent>();
		}

		private void Start()
		{
			Data.ShipData shipData = GameData.Instance.ships.GetShipData( _shipType );
			if( shipData != null )
			{
				ShipStats shipStats = GameData.Instance.stats.GetShipStats( shipData.type, shipData.multipliers );
				if( shipStats != null )
					Setup( shipStats );
			}

			_health.onDie.AddListener( OnDie );
		}

		private void Setup( ShipStats shipStats )
		{
			_health.Init( shipStats.health );
			_evasionDamageReceiver.Init( shipStats.mobility / 100f );
			_movement.speed = shipStats.moveSpeed;
			_weapon.shootDelay = shipStats.shootSpeed;
			_weapon.damage = shipStats.weaponDamage;
		}

		private void OnDie()
		{
			GameObjectsPool.Destroy( gameObject );
		}
	}
}