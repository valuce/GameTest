﻿using UnityEngine;
using System.Collections;

namespace GameTest
{

	public class BonusUseShield : BonusUseComponent
	{
		private GameObject _shield;

		public override void Use( float useTime, float value )
		{
			base.Use( useTime, value );

			GameObject Prefab = Assets.GetAsset<GameObject>( "Shield", "Game" );
			if( Prefab )
			{
				_shield = GameObjectsPool.Instantiate( Prefab, transform, false );
				_shield.transform.localPosition = Vector3.zero;
			}
		}

		protected override void OnStopUse()
		{
			base.OnStopUse();

			GameObjectsPool.Destroy( _shield );
		}
	}
}