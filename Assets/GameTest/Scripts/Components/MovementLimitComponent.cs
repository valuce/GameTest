﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameTest
{

	public class MovementLimitComponent : MonoBehaviour
	{
		[SerializeField]
		private Vector2 _limit;

		private Transform _thisTransform;

		private void Awake()
		{
			_thisTransform = transform;
		}

		private void LateUpdate()
		{
			Vector3 position= _thisTransform.localPosition;
			position.x = Mathf.Clamp( position.x, -_limit.x, _limit.x );
			position.y = Mathf.Clamp( position.y, -_limit.y, _limit.y );
			_thisTransform.localPosition = position;
		}
	}
}