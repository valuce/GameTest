﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameTest
{
	public class WeaponComponent : MonoBehaviour
	{
		[SerializeField]
		private GameObject BulletPrefab;
		[SerializeField]
		private Transform _trunk;
		
		public float shootDelay = 0.5f;
		public float damage = 10f;

		private float _timer = 0f;		

		public bool ready
		{
			get { return _timer >= shootDelay; }
		}

		private void Start()
		{
			_timer= shootDelay;
		}

		private void Update()
		{
			_timer += Time.deltaTime;
		}

		public void Shoot()
		{
			if( ready )
			{
				var bulletGO = GameObjectsPool.Instantiate( BulletPrefab, _trunk.position, _trunk.rotation );
				var bullet = bulletGO.GetComponent<BulletComponent>();
				bullet.damage = damage;
				bulletGO.layer = gameObject.layer;

				_timer = 0;
			}
		}
	}
}