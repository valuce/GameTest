﻿using UnityEngine;
using System.Collections;

namespace GameTest
{

	public class BonusUseShootingSpeed : BonusUseComponent
	{
		private WeaponComponent _weapon;
		private float _prevValue;

		private void Awake()
		{
			_weapon = GetComponent<WeaponComponent>();
		}

		public override void Use( float useTime, float value )
		{
			base.Use( useTime, value );

			if( _weapon )
			{
				_prevValue = _weapon.shootDelay;
				_weapon.shootDelay /= value;
			}
		}

		protected override void OnStopUse()
		{
			base.OnStopUse();

			if( _weapon )
				_weapon.shootDelay = _prevValue;
		}
	}
}