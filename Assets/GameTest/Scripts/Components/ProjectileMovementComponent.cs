﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameTest
{

	public class ProjectileMovementComponent : MovementComponent
	{
		private Rigidbody2D _rigidbody;

		public Vector3 direction = Vector3.down;

		private void Awake()
		{
			_rigidbody = GetComponent<Rigidbody2D>();
		}

		private void Start()
		{
			Move( transform.rotation * direction );
		}

		public override void Move( Vector3 direction )
		{
			if( _rigidbody )
				_rigidbody.velocity = direction * speed;
		}
	}
}