﻿using UnityEngine;

namespace GameTest
{
	public class ShipAnimsComponent : MonoBehaviour
	{
		private Animator _animator;

		private void Awake()
		{
			_animator = GetComponent<Animator>();
		}

		private void Start()
		{
			Invoke( "Idle", Random.value );
		}

		public void Idle()
		{
			SafePlay( "Idle" );
		}

		public void Dodged()
		{
			SafePlay( "Dodged" );
		}

		private void SafePlay( string animName )
		{
			if( _animator )
				_animator.Play( animName );
		}
	}
}
