﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameTest
{
	public class PlayerController : MonoBehaviour
	{
		private MovementComponent _movement;
		private WeaponComponent _weapon;

		private void Awake()
		{
			_movement = GetComponent<MovementComponent>();
			_weapon = GetComponent<WeaponComponent>();
		}

		private void Update()
		{
			if( Time.deltaTime == 0f )
				return;

			float horizontal = Input.GetAxis( "Horizontal" );
			Vector3 dir = new Vector3( horizontal, 0f, 0f );

			if( _movement )
				_movement.Move( dir );

			if( Input.GetButtonDown("Fire1") )
			{
				if( _weapon )
					_weapon.Shoot();
			}
		}
	}
}