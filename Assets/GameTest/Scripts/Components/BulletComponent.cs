﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace GameTest
{

	public class BulletComponent : MonoBehaviour
	{
		public float damage = 10f;

		[SerializeField]
		private UnityEvent _onHit;
		public UnityEvent onHit { get { return _onHit; } }

		private void OnTriggerEnter2D( Collider2D collision )
		{
			DamageReceiverComponent damageReceiver = collision.GetComponent<DamageReceiverComponent>();
			if( damageReceiver )
			{
				if( damageReceiver.TakeDamage( damage ) )
					OnHit();
			}
		}

		virtual protected void OnHit()
		{
			_onHit.Invoke();

			GameObjectsPool.Destroy( gameObject );
		}
	}
}
