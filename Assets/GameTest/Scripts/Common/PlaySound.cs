﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameTest
{
	public class PlaySound : MonoBehaviour
	{
		[SerializeField]
		private AudioClip _audioClip;
		[SerializeField]
		private bool _playOnStart = false;

		private void Start()
		{
			if( _playOnStart )
				Play();
		}

		public void Play()
		{
			SoundsManager.Instance.Play( _audioClip );
		}
	}
}