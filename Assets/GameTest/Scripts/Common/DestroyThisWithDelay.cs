﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameTest
{

	public class DestroyThisWithDelay : MonoBehaviour
	{
		public float lifeTime;

		private float _timer;

		void Start()
		{
			_timer = 0;
		}

		private void Update()
		{
			_timer += Time.deltaTime;
			if( _timer >= lifeTime )
				Destroy();
		}

		private void Destroy()
		{
			GameObjectsPool.Destroy( gameObject );
		}
	}
}