﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameTest
{
	static public class Assets
	{
		private static Dictionary<string, AssetsPack> _assets = new Dictionary<string, AssetsPack>();

		static public void AddAssetsPack( AssetsPack pack )
		{
			if( pack )
				_assets.Add( pack.alias, pack );
		}

		static public void UnloadAssetsPack( AssetsPack pack )
		{
			if( pack )
				_assets.Remove( pack.alias );
		}

		static public T GetAsset<T>( string assetName, string packName ) where T : Object
		{
			if( string.IsNullOrEmpty( assetName ) ||  string.IsNullOrEmpty( packName ) )
				return null;

			AssetsPack pack;
			if( _assets.TryGetValue( packName, out pack ) )
				return pack.GetAsset<T>(assetName);

			return null;
		}
	}
}