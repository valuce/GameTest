﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameTest
{
	public class SpawnObject : MonoBehaviour
	{
		public GameObject Prefab;
		public bool spawnOnStart = false;

		private void Start()
		{
			if( spawnOnStart )
				Spawn();
		}

		public void Spawn()
		{
			if( Prefab )
				GameObjectsPool.Instantiate( Prefab, transform.position, transform.rotation );
		}
	}
}
