﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameTest
{

	public class GameObjectsPool : MonoBehaviour
	{
		static private GameObjectsPool Current;
		private Dictionary<int, ObjectsPool> _pools = new Dictionary<int, ObjectsPool>();

		#region Instantiate and Destroy
		static public GameObject Instantiate( GameObject Prefab, Vector3 position, Quaternion rotation )
		{
			if( Prefab == null )
				return null;

			if( Current == null )
				return Object.Instantiate( Prefab, position, rotation );

			return _Instantiate( Prefab, Current.transform, false, position, rotation );
		}

		static public GameObject Instantiate( GameObject Prefab )
		{
			if( Prefab == null )
				return null;

			if( Current == null )
				return Object.Instantiate( Prefab );

			return _Instantiate( Prefab, Current.transform, false, Prefab.transform.position, Prefab.transform.rotation );			
		}

		static public GameObject Instantiate( GameObject Prefab, Transform parent )
		{
			if( Prefab == null )
				return null;

			if( Current == null )
				return Object.Instantiate( Prefab, parent );

			return _Instantiate( Prefab, parent, false, Prefab.transform.position, Prefab.transform.rotation );			
		}

		static public GameObject Instantiate( GameObject Prefab, Transform parent, bool instantiateInWorldSpace )
		{
			if( Prefab == null )
				return null;

			if( Current == null )
				return Object.Instantiate( Prefab, parent, instantiateInWorldSpace );

			return _Instantiate( Prefab, parent, instantiateInWorldSpace, Prefab.transform.position, Prefab.transform.rotation );
		}

		static private GameObject _Instantiate( GameObject Prefab, Transform parent, bool instantiateInWorldSpace, Vector3 position, Quaternion rotation )
		{
			ObjectsPool pool = Current.GetObjectsPool( Prefab );
			GameObject go = pool.Take();
			if( go == null )
			{
				go = Object.Instantiate( Prefab, parent, instantiateInWorldSpace );
				pool.AddNewObject( go );
				go.transform.position = position;
				go.transform.rotation = rotation;
			}
			else
			{
				go.transform.position = position;
				go.transform.rotation = rotation;
				go.transform.SetParent( parent, instantiateInWorldSpace );
				go.SetActive( true );
				go.SendMessage( "Start", SendMessageOptions.DontRequireReceiver );
			}

			return go;
		}

		static public void Destroy( GameObject gameObject )
		{
			if( gameObject == null )
				return;

			if( Current != null && Current.enabled )
			{
				if( Current.ReturnToPool( gameObject ) )
				{
					gameObject.transform.SetParent( Current.transform );
					gameObject.SendMessage( "OnDestroy", SendMessageOptions.DontRequireReceiver );
					gameObject.SetActive( false );
					return;
				}
			}

			Object.Destroy( gameObject );
		}		

		static public void Destroy( GameObject gameObject, float t )
		{
			if( gameObject == null )
				return;

			if( Current != null )
			{
				Current.StartCoroutine( Current.DestroyDelay( gameObject, t) );
				return;
			}

			Object.Destroy( gameObject, t );
		}


		#endregion Instantiate and Destroy



		private void Awake()
		{
			Current = this;
		}

		private IEnumerator DestroyDelay( GameObject go, float t )
		{
			yield return new WaitForSeconds( t );
			GameObjectsPool.Destroy( go );
		}


		private ObjectsPool GetObjectsPool( GameObject Prefab )
		{
			int prefabId = Prefab.GetInstanceID();
			ObjectsPool pool;
			if( _pools.TryGetValue( prefabId, out pool ) )
				return pool;

			pool = new ObjectsPool();
			_pools.Add( prefabId, pool );
			return pool;
		}

		
		private bool ReturnToPool( GameObject go )
		{
			int instanceID = go.GetInstanceID();

			var pools = _pools.Values;
			foreach( ObjectsPool pool in pools )
			{
				if( pool.ContainsId( instanceID ) )
				{
					pool.Return( go );
					return true;
				}
			}

			return false;
		}


		class ObjectsPool
		{
			private Stack<GameObject> _objects = new Stack<GameObject>();
			private HashSet<int> _ids = new HashSet<int>();			

			public GameObject Take()
			{
				if( _objects.Count > 0 )
				{
					return _objects.Pop();
				}

				return null;
			}

			public void AddNewObject( GameObject go )
			{
				_ids.Add( go.GetInstanceID() );
			}

			public void Return( GameObject go )
			{
				_objects.Push( go );
			}

			public bool ContainsId( int id )
			{
				return _ids.Contains( id );
			}
		}
	}
}