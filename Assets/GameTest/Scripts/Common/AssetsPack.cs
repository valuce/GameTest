﻿using UnityEngine;
using System.Collections.Generic;

namespace GameTest
{
	[CreateAssetMenu( fileName = "AssetsPack", menuName = "GameTest/AssetsPack" )]
	public class AssetsPack : ScriptableObject
	{
		[System.Serializable]
		private class AssetAlias
		{
			public string alias = string.Empty;
			public Object asset = null;
		}

		[SerializeField]
		private string _alias;
		public string alias { get { return _alias; } }

		[SerializeField]
		private List<Object> _assets = new List<Object>();
		[SerializeField]
		private List<AssetAlias> _assetsAlias = new List<AssetAlias>();

		private Dictionary<string, Object> _assetsMap = new Dictionary<string, Object>();

		private void CacheInMap()
		{
			_assetsMap.Clear();

			foreach( AssetAlias data in _assetsAlias )
			{
				if( data.asset )
				{
					if( string.IsNullOrEmpty( data.alias ) )
						AddAsset( data.asset );
					else
						AddAsset( data.alias, data.asset );
				}
			}

			foreach( Object asset in _assets )
			{
				if( asset )
					AddAsset( asset );
			}
		}

		private Object GetAsset( string assetName )
		{
			if( string.IsNullOrEmpty( assetName ) )
				return null;

			if( _assetsMap.Count == 0 )
				CacheInMap();

			Object asset;
			if( _assetsMap.TryGetValue( assetName, out asset ) )
				return asset;

			//asset = Resources.Load( assetName );
			//if( asset )
			//{
			//	AddAsset( asset );
			//	return asset;
			//}

			return null;
		}

		public T GetAsset<T>( string assetName ) where T : Object
		{
			if( string.IsNullOrEmpty( assetName ) )
				return null;

			Object asset = GetAsset( assetName );
			return asset ? asset as T : null;
		}

		private void AddAsset( Object asset )
		{
			if( asset )
				AddAsset( asset.name, asset );
		}

		private void AddAsset( string assetName, Object asset )
		{
			if( asset && !string.IsNullOrEmpty( assetName ) )
				_assetsMap.Add( assetName, asset );
		}
	}
}