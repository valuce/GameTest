﻿using UnityEngine;

namespace GameTest
{
	public abstract class SingletonBehaviour<T> : MonoBehaviour where T : MonoBehaviour
	{
		protected static T _instance = null;
		public static T Instance { get { return _instance ?? FindOrCreateInstance(); } }
		private static T FindOrCreateInstance()
		{
			_instance = FindObjectOfType<T>();
			if( _instance == null )
			{
				string typeName = typeof( T ).Name;

				GameObject go = LoadFromResource( typeName );
				if( go == null )
				{
					go = new GameObject();
					_instance = go.AddComponent<T>();
				}
				else
				{
					_instance = go.GetComponent<T>();
				}
				go.name = typeName;
				DontDestroyOnLoad( go );
			}
			return _instance;
		}

		private static GameObject LoadFromResource( string typeName )
		{
			GameObject go = Resources.Load<GameObject>( typeName );
			if( go && go.GetComponent<T>() )
				return Instantiate( go );
			return null;
		}
	}
}