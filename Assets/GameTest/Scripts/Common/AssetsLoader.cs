﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameTest
{

	public class AssetsLoader : MonoBehaviour
	{
		[SerializeField]
		private AssetsPack assetsPack;

		private void Awake()
		{
			Assets.AddAssetsPack( assetsPack );
		}

		private void OnDestroy()
		{
			Assets.UnloadAssetsPack( assetsPack );
		}
	}
}