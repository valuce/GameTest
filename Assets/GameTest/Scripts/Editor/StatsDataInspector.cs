﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace GameTest
{
	[CustomEditor( typeof( Data.StatsData ) )]
	public class StatsDataInspector : Editor
	{
		private SerializedObject _serializedTarget;

		void OnEnable()
		{
			Data.StatsData data = (Data.StatsData)target;

			_serializedTarget = new SerializedObject( data );
		}

		public override void OnInspectorGUI()
		{
			if( _serializedTarget == null )
				return;

			_serializedTarget.Update();

			EditorGUIUtility.ShowListProperty( "Base Ships Stats", _serializedTarget.FindProperty( "ships" ) );

			EditorGUIUtility.ShowListProperty( "Ships Stats Multipliers", _serializedTarget.FindProperty( "multiplierStats" ) );

			_serializedTarget.ApplyModifiedProperties();
		}
	}
}