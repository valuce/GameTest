﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace GameTest
{
	public sealed class EditorGUIUtility
	{
		public static void ShowListProperty( string label, SerializedProperty list )
		{
			GUILayout.BeginHorizontal();
			GUILayout.FlexibleSpace();
			GUILayout.Label( label, EditorStyles.whiteBoldLabel );
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();

			_ShowList( list );

			if( GUILayout.Button( "Add" ) )
			{
				list.InsertArrayElementAtIndex( list.arraySize );
			}
		}

		private static void _ShowList( SerializedProperty list )
		{
			for( int i = 0; i < list.arraySize; i++ )
			{
				GUILayout.BeginHorizontal();
				EditorGUILayout.PropertyField( list.GetArrayElementAtIndex( i ), true );
				if( GUILayout.Button( "Delete" ) )
				{
					list.DeleteArrayElementAtIndex( i );
				}
				GUILayout.EndHorizontal();
			}
		}
	}
}