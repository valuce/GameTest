﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace GameTest
{
	[CustomEditor( typeof( Data.BonusesData ) )]
	public class BonusesDataInspector : Editor
	{
		private SerializedObject _serializedTarget;

		void OnEnable()
		{
			Data.BonusesData data = (Data.BonusesData)target;

			_serializedTarget = new SerializedObject( data );
		}

		public override void OnInspectorGUI()
		{
			if( _serializedTarget == null )
				return;

			_serializedTarget.Update();

			EditorGUIUtility.ShowListProperty( "Bonuses", _serializedTarget.FindProperty( "bonuses" ) );

			_serializedTarget.ApplyModifiedProperties();
		}
	}
}