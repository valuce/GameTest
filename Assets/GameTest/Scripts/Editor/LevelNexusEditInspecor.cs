﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace GameTest
{
	[CustomEditor( typeof( LevelNexusEdit ) )]
	public class LevelNexusEditInspecor : Editor
	{

		public override void OnInspectorGUI()
		{
			DrawDefaultInspector();

			LevelNexusEdit myTarget = (LevelNexusEdit)target;

			if( GUILayout.Button( "Clear" ) )
			{
				myTarget.Clear();
			}

			if( GUILayout.Button("Load") )
			{
				myTarget.Load();
			}

			if( GUILayout.Button( "Save" ) )
			{
				myTarget.Save();
			}
		}
	}
}