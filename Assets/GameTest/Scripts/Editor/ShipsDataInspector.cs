﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace GameTest
{
	[CustomEditor( typeof( Data.ShipsData ) )]
	public class ShipsDataInspector : Editor
	{
		private SerializedObject _serializedTarget;

		void OnEnable()
		{
			Data.ShipsData data = (Data.ShipsData)target;

			_serializedTarget = new SerializedObject( data );
		}

		public override void OnInspectorGUI()
		{
			_serializedTarget.Update();

			EditorGUIUtility.ShowListProperty( "Ships", _serializedTarget.FindProperty( "ships" ) );

			_serializedTarget.ApplyModifiedProperties();
		}
	}
}