﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using GameTest.Data;

namespace GameTest
{
	class StatsWindow : EditorWindow
	{
		private StatsData _statsData;
		private SerializedObject _serializedObject;

		private void Awake()
		{
			_statsData = Resources.Load<StatsData>( "Data/Stats" );
			_serializedObject = new SerializedObject( _statsData );
		}

		[MenuItem( "GameTest/Stats" )]
		public static void ShowWindow()
		{
			GetWindow<StatsWindow>( "Stats" );
		}

		void OnGUI()
		{
			_serializedObject.Update();

			EditorGUIUtility.ShowListProperty( "Base Ships Stats", _serializedObject.FindProperty( "ships" ) );

			EditorGUIUtility.ShowListProperty( "Ships Stats Multipliers", _serializedObject.FindProperty( "multiplierStats" ) );

			_serializedObject.ApplyModifiedProperties();
		}
	}
}