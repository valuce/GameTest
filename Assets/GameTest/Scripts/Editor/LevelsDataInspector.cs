﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace GameTest
{
	[CustomEditor( typeof( Data.LevelsData ) )]
	public class LevelsDataInspector : Editor
	{
		private SerializedObject _serializedTarget;

		void OnEnable()
		{
			Data.LevelsData data = (Data.LevelsData)target;

			_serializedTarget = new SerializedObject( data );
		}

		public override void OnInspectorGUI()
		{
			if( _serializedTarget == null )
				return;

			_serializedTarget.Update();

			EditorGUIUtility.ShowListProperty( "Levels", _serializedTarget.FindProperty( "levels" ) );

			_serializedTarget.ApplyModifiedProperties();
		}
	}
}