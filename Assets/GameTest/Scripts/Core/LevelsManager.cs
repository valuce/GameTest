﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameTest.Data;

namespace GameTest
{	
	public class LevelsManager
	{
		private List<LevelData> _levels = new List<LevelData>();

		public int levelsCount
		{
			get { return _levels.Count; }
		}

		public void Init( LevelsData levelsData )
		{
			_levels.Clear();
			_levels.AddRange( levelsData.levels );
		}

		public LevelData GetLevel( int index )
		{
			if( index >= 0 && index < levelsCount )
				return _levels[index];

			return null;
		}
	}
}