﻿using UnityEngine;
using System.Collections.Generic;

namespace GameTest
{

	public class ShipStats
	{
		public readonly float health = 100f;
		public readonly float shootSpeed = 0.5f;
		public readonly float moveSpeed = 3f;
		public readonly float mobility = 15f;
		public readonly float weaponDamage = 10f;

		public ShipStats() { }

		public ShipStats( float health, float shootSpeed, float moveSpeed, float mobility, float weaponDamage )
		{
			this.health = health;
			this.shootSpeed = shootSpeed;
			this.moveSpeed = moveSpeed;
			this.mobility = mobility;
			this.weaponDamage = weaponDamage;
		}
	}

	public class ShipStatsManager
	{
		Dictionary<string, Data.BaseShipStatsData> _baseShipStats = new Dictionary<string, Data.BaseShipStatsData>();

		Dictionary<string, Data.ShipStatsMultipliersData> _shipStatsMultipliers = new Dictionary<string, Data.ShipStatsMultipliersData>();

		public ShipStats GetShipStats( string shipType, string multiplierType )
		{
			Data.BaseShipStatsData baseStats;
			if( _baseShipStats.TryGetValue(shipType, out baseStats) )
			{
				Data.ShipStatsMultipliersData statsMultipliers;
				if( _shipStatsMultipliers.TryGetValue( multiplierType, out statsMultipliers ) )
				{
					return new ShipStats( 
						baseStats.health * statsMultipliers.health, 
						baseStats.shootSpeed / statsMultipliers.shootSpeed,
						baseStats.moveSpeed * statsMultipliers.moveSpeed,
						baseStats.mobility * statsMultipliers.mobility,
						baseStats.weaponDamage * statsMultipliers.weaponDamage );
				}
				else
				{
					Debug.LogError( "ShipsManager.GetShipStats - not statsMultipliers: " + statsMultipliers );
				}
			}
			else
			{
				Debug.LogError( "ShipsManager.GetShipStats - not shipType: " + shipType );
			}

			return new ShipStats();
		}


		public void Init( Data.StatsData shipsData )
		{
			_baseShipStats.Clear();
			_shipStatsMultipliers.Clear();

			foreach( var baseStats in shipsData.ships )
			{
				_baseShipStats.Add( baseStats.type, baseStats );
			}

			foreach( var multiplierStats in shipsData.multiplierStats )
			{
				_shipStatsMultipliers.Add( multiplierStats.type, multiplierStats );
			}
		}
	}
}