﻿using UnityEngine;
using System.Collections.Generic;
using GameTest.Data;

namespace GameTest
{

	public class BonusesManager
	{
		public List<BonusData> _bonuses = new List<BonusData>();

		public void Init( Data.BonusesData bonusesData )
		{
			_bonuses.Clear();
			_bonuses.AddRange( bonusesData.bonuses );
		}

		public GameObject GetRandomBonus()
		{
			int chanceAll = 0;
			foreach( var bonus in _bonuses )
				chanceAll += bonus.chance;

			int chance = Random.Range( 0, chanceAll );
			foreach( var item in _bonuses )
			{
				if( item.chance >= chance )
					return item.Prefab;
				else
					chance -= item.chance;
			}

			return null;
		}
	}
}