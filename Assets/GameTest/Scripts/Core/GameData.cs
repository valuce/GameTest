﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameTest
{

	public class GameData : SingletonBehaviour<GameData>
	{
		[SerializeField]
		private Data.StatsData _statsData;
		[SerializeField]
		private Data.BonusesData _bonusesData;
		[SerializeField]
		private Data.LevelsData _levelsData;
		[SerializeField]
		private Data.ShipsData _shipsData;

		public ShipStatsManager stats { private set; get; }
		public ShipsManager ships { private set; get; }
		public LevelsManager levels { private set; get; }
		public BonusesManager bonuses { private set; get; }

		[HideInInspector]
		public string selectedPlayer = "Player1";
		[HideInInspector]
		public int currentLevel = 0;

		private void Awake()
		{
			if( _instance == null )
			{
				_instance = this;
				DontDestroyOnLoad( gameObject );
			}
			else
			{
				Destroy( gameObject );
				return;
			}

			stats = new ShipStatsManager();
			stats.Init( _statsData );

			ships = new ShipsManager();
			ships.Init( _shipsData );

			levels = new LevelsManager();
			levels.Init( _levelsData );

			bonuses = new BonusesManager();
			bonuses.Init( _bonusesData );
		}

		public void Load()
		{

		}

		public void Save()
		{

		}
	}
}