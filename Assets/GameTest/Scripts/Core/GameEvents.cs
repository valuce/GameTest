﻿using UnityEngine;
using UnityEngine.Events;

namespace GameTest
{
	static public class GameEvents
	{
		static public event UnityAction<GameObject> onPlayerCreated;

		static public event UnityAction onGameOver;
		static public event UnityAction onLevelComplete;


		static public void PlayerCreated( GameObject player )
		{
			if( onPlayerCreated != null )
				onPlayerCreated.Invoke( player );
		}

		static public void GameOver()
		{
			if( onGameOver != null )
				onGameOver.Invoke();
		}

		static public void LevelComplete()
		{
			if( onLevelComplete != null )
				onLevelComplete.Invoke();
		}
	}
}