﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

namespace GameTest
{
	public class SoundsManager : SingletonBehaviour<SoundsManager>
	{
		[SerializeField]
		private AudioMixer _audioMixer;
		[SerializeField]
		private AudioSource _sfxAudioSource;
		private HashSet<AudioClip> _plaingClips = new HashSet<AudioClip>();
		public float musicVolume
		{
			get
			{
				return PlayerPrefs.GetFloat( "MusicVolume", 0.8f );
			}
			set
			{
				float volume = Mathf.Clamp01( value );
				PlayerPrefs.SetFloat( "MusicVolume", volume );

				if( _audioMixer )
					_audioMixer.SetFloat( "Music", Mathf.Lerp( -80f, 0f, value ) );
			}
		}

		public float sfxVolume
		{
			get
			{
				return PlayerPrefs.GetFloat( "SfxVolume", 0.8f );
			}
			set
			{
				float volume = Mathf.Clamp01( value );
				PlayerPrefs.SetFloat( "SfxVolume", volume );


				if( _audioMixer )
					_audioMixer.SetFloat( "Sfx", Mathf.Lerp( -80f, 0f, value ) );
			}
		}

		public void Apply()
		{
			musicVolume = musicVolume;
			sfxVolume = sfxVolume;
		}

		private void Awake()
		{
			if( _instance == null )
			{
				_instance = this;
				DontDestroyOnLoad( gameObject );
			}
			else
			{
				Destroy( gameObject );
				return;
			}
		}

		private void Start()
		{
			Apply();
		}

		private void LateUpdate()
		{
			_plaingClips.Clear();
		}

		public void Play( AudioClip clip )
		{
			if( !_plaingClips.Add(clip) )
				return;

			if( _sfxAudioSource )
				_sfxAudioSource.PlayOneShot( clip );
		}
	}
}