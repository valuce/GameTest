﻿using UnityEngine;
using System.Collections.Generic;
using GameTest.Data;

namespace GameTest
{
	public class ShipsManager
	{
		private Dictionary<string, ShipData> _shipsMap = new Dictionary<string, ShipData>();

		public void Init( ShipsData shipsData  )
		{
			_shipsMap.Clear();
			foreach( var data in shipsData.ships )
				_shipsMap.Add( data.name, data );
		}

		public ShipData GetShipData( string shipName )
		{
			ShipData shipData;
			if( _shipsMap.TryGetValue( shipName, out shipData ) )
			{
				return shipData;
			}

			Debug.LogError( "ShipsManager.GetShipData - not find ship: " + shipName );
			return null;
		}

		public List<ShipData> GetShipsWithType( string shipType )
		{
			List<ShipData> list = new List<ShipData>();
			foreach( var pair in _shipsMap )
			{
				if( pair.Value.type == shipType )
					list.Add( pair.Value );
			}

			return list;
		}
	}
}