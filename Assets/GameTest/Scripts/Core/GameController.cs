﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameTest
{
	public enum GameState
	{
		None, MainMenu, Lobby, Game, LevelWIn, GameOver
	}

	public class GameController : SingletonBehaviour<GameController>
	{
		public GameState currentGameState { private set; get; }
		public event System.Action<GameState, GameState> onChangeGameState;

		private void Awake()
		{
			if( _instance == null )
			{
				_instance = this;
				DontDestroyOnLoad( gameObject );
			}
			else
			{
				Destroy( gameObject );
				return;
			}

			Application.targetFrameRate = 60;			
		}

		private void Start()
		{
			SoundsManager.Instance.Apply();
			GameData.Instance.Load();

			currentGameState = GameState.MainMenu;

#if UNITY_EDITOR
			switch( UnityEngine.SceneManagement.SceneManager.GetActiveScene().name )
			{
				case "GameScene":
					currentGameState = GameState.Game;
					break;
				case "MainMenuScene":
					currentGameState = GameState.MainMenu;
					break;
				case "Lobby":
					currentGameState = GameState.Lobby;
					break;
			}
#endif
		}

		public void GotoState( GameState newState )
		{
			if( currentGameState == newState )
				return;

			GameState prevState = currentGameState;
			currentGameState = newState;

			switch( newState )
			{
				case GameState.MainMenu :
					LoadScene( "MainMenuScene" );
					break;

				case GameState.Lobby:
					GameData.Instance.currentLevel = 0;
					LoadScene( "LobbyScene" );
					break;

				case GameState.Game:
					LoadScene( "GameScene" );
					break;

				case GameState.GameOver:
				case GameState.LevelWIn:					
					break;
			}

			if( onChangeGameState != null )
			{
				onChangeGameState.Invoke( prevState, newState );
			}
		}

		public void Quit()
		{
#if UNITY_EDITOR
			UnityEditor.EditorApplication.isPlaying = false;
#else
			Application.Quit();
#endif
		}


		private void LoadScene( string sceneName )
		{
			UnityEngine.SceneManagement.SceneManager.LoadScene( sceneName );
		}
	}
}
