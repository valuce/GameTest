﻿using UnityEngine;
using System.Collections.Generic;

namespace GameTest.Data
{
	[System.Serializable]
	public class BaseShipStatsData
	{
		public string type = "Player";
		public float health = 100f;
		public float shootSpeed = 0.5f;
		public float moveSpeed = 3f;
		public float mobility = 15f;
		public float weaponDamage = 10f;
	}

	[System.Serializable]
	public class ShipStatsMultipliersData
	{
		public string type= "Player1";
		public float health = 1f;
		public float shootSpeed = 1f;
		public float moveSpeed = 1f;
		public float mobility = 1f;
		public float weaponDamage = 1f;
	}

	[CreateAssetMenu(fileName = "StatsData", menuName = "GameTest/StatsData" )]
	public class StatsData : ScriptableObject
	{
		public List<BaseShipStatsData> ships = new List<BaseShipStatsData>();
		public List<ShipStatsMultipliersData> multiplierStats = new List<ShipStatsMultipliersData>();
	}
}
