﻿using UnityEngine;
using System.Collections.Generic;

namespace GameTest.Data
{
	[System.Serializable]
	public class ShipData
	{
		public string name;
		public string type;
		public string multipliers;
		public GameObject Prefab;
	}

	[CreateAssetMenu( fileName = "ShipsData", menuName = "GameTest/ShipsData" )]
	public class ShipsData : ScriptableObject
	{
		public List<ShipData> ships;
	}

}