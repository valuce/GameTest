﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameTest.Data
{
	[System.Serializable]
	public class EnemyLevelPositionData
	{
		public string enemyName;
		public Vector3 position;
	}

	[CreateAssetMenu( fileName = "LevelNexus", menuName = "GameTest/LevelNexus" )]
	public class LevelNexus : ScriptableObject
	{
		public List<EnemyLevelPositionData> enemies;
	}
}