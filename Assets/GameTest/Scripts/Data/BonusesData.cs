﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameTest.Data
{
	[System.Serializable]
	public class BonusData
	{
		public string type;
		public int chance = 0;
		public GameObject Prefab;
	}

	[CreateAssetMenu( fileName = "BonusesData", menuName = "GameTest/BonusesData" )]
	public class BonusesData : ScriptableObject
	{
		public List<BonusData> bonuses;
		
	}
}