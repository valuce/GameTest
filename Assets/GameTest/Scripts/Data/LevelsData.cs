﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameTest.Data
{
	[System.Serializable]
	public class LevelData
	{
		public string name;
		public GameObject background;
		public LevelNexus nexus;
	}

	[CreateAssetMenu( fileName = "LevelsData", menuName = "GameTest/LevelsData" )]
	public class LevelsData : ScriptableObject
	{
		public List<LevelData> levels;
	}
}